import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-resultes',
  templateUrl: './resultes.component.html',
  styleUrls: ['./resultes.component.scss'],
})
export class ResultesComponent implements OnInit {
  headers = [
    { key: 'task', title: 'Task' },
    { key: 'assigned_users', title: 'Assigned Users', type: 'img' },
    { key: 'status', title: 'Status' },
    { key: 'start_date', title: 'Start Date', type: 'date' },
    { key: 'priorityLevel', title: 'PriorityLevel' },
  ];

  workflows: any[] = [
    {
      id: 1,
      task: 'Document 1',
      assigned_users: './avatar',
      status: 'Pages 1',
      start_date: 'mm / dd / yy',
      priorityLevel: 4,
    },
    {
      id: 2,
      task: 'Document 2',
      assigned_users: './avatar',
      status: 'Pages 2',
      start_date: 'mm / dd / yy',
      priorityLevel: 5,
    },
    {
      id: 3,
      task: 'Document 5',
      assigned_users: './avatar',
      status: 'Pages 1',
      start_date: 'mm / dd / yy',
      priorityLevel: 4,
    },
    {
      id: 1,
      task: 'Document 1',
      assigned_users: './avatar',
      status: 'Pages 1',
      start_date: 'mm / dd / yy',
      priorityLevel: 4,
    },
    {
      id: 1,
      task: 'Document 1',
      assigned_users: './avatar',
      status: 'Pages 1',
      start_date: 'mm / dd / yy',
      priorityLevel: 4,
    },
  ];

  constructor() {}

  ngOnInit() {
    this.workflows = this.workflows.map(workflow => {
      workflow.assigned_users = {
        value: workflow.assigned_users,
        template: '<i class="fa fa-check"></i>',
      };

      workflow.priorityLevel = {
        value: workflow.priorityLevel,
        template: this.getPriorityLevelTpl(workflow.priorityLevel as number),
      };

      return workflow;
    });
  }

  private getPriorityLevelTpl(priorityLevel: number) {
    const maxPriorityLevel = Array(priorityLevel).fill('<i class="fa fa-star-o"></i>');

    return maxPriorityLevel
      .map((v, i) => (i + 1 < 3 ? v : '<i class="fa fa-star"></i>'))
      .reverse();
  }
}

import { Routes } from '@angular/router';
import { SearchComponent } from './search.component';
import { ResultesComponent } from './resultes/resultes.component';

export const routes: Routes = [
  {
    path: '',
    component: SearchComponent,
    children: [
      {
        path: 'results',
        component: ResultesComponent,
      },
    ],
  },
];

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { MaterialModule } from '../../shared/material.module';
import { SearchComponent } from './search.component';
import { routes } from './search.routes';
import { ResultesComponent } from './resultes/resultes.component';

@NgModule({
  imports: [SharedModule, MaterialModule, RouterModule.forChild(routes)],
  declarations: [SearchComponent, ResultesComponent],
  exports: [],
})
export class SearchModule {}

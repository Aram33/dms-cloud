import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home.component';
import { AuthorizedApplicationsComponent } from './authorized-applications/authorized-applications.component';

export const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
      {
        path: 'authorized-applications',
        component: AuthorizedApplicationsComponent,
      },
      {
        path: 'users-and-groups',
        loadChildren: () =>
          import('./users-and-groups/users-and-groups.module').then(
            m => m.UsersAndGroups,
          ),
      },
      {
        path: 'workflow',
        loadChildren: () =>
          import('./workflow/workflow.module').then(m => m.WorkFlowModule),
      },
      {
        path: 'favorites',
        loadChildren: () =>
          import('./favorites/favorites.module').then(m => m.FavoritesModule),
      },
    ],
  },
  { path: 'home', redirectTo: 'home/dashboard', pathMatch: 'full' },
];

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './home.routes';
import { HomeComponent } from './home.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { SharedModule } from '@shared/shared.module';
import { AuthorizedApplicationsComponent } from './authorized-applications/authorized-applications.component';

@NgModule({
  imports: [SharedModule, DashboardModule, RouterModule.forChild(routes)],
  declarations: [HomeComponent, AuthorizedApplicationsComponent],
  exports: [],
})
export class HomeModule {}

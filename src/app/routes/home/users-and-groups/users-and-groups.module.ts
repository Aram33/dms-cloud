import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { routes } from './users-and-groups.routes';
import { GropusComponent } from './components/gropus/gropus.component';
import { UsersComponent } from './components/users/users.component';
import { UsersAndGroupsComponent } from './users-and-groups.component';

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [UsersAndGroupsComponent, GropusComponent, UsersComponent],
  exports: [],
})
export class UsersAndGroups {
  constructor() {}
}

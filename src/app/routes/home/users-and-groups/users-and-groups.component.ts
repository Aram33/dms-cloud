import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users-and-groups',
  templateUrl: './users-and-groups.component.html',
  styleUrls: ['./users-and-groups.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UsersAndGroupsComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit(): void {
    this.router.navigate(['home', 'users-and-groups', 'users']);
  }
}

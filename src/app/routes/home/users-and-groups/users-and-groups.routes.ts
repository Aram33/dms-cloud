import { Routes } from '@angular/router';
import { UsersAndGroupsComponent } from './users-and-groups.component';
import { UsersComponent } from './components/users/users.component';
import { GropusComponent } from './components/gropus/gropus.component';

export const routes: Routes = [
  {
    path: 'home/users-and-groups',
    redirectTo: 'home/dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: UsersAndGroupsComponent,
    children: [
      {
        path: 'users',
        component: UsersComponent,
      },
      {
        path: 'groups',
        component: GropusComponent,
      },
    ],
  },
];

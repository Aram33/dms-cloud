import { Component, OnInit } from '@angular/core';

class User {
  constructor(
    public id: number,
    public employe_no: string,
    public username: string,
    public employe_name: string,
  ) {}
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  constructor() {}
  users: User[] = [
    {
      id: 1,
      employe_name: 'John Doe',
      employe_no: '000-00',
      username: 'jDoe',
    },
    {
      id: 2,
      employe_name: 'John Doe',
      employe_no: '000-00',
      username: 'jDoe',
    },
    {
      id: 3,
      employe_name: 'John Doe',
      employe_no: '000-00',
      username: 'jDoe',
    },
    {
      id: 4,
      employe_name: 'John Doe',
      employe_no: '000-00',
      username: 'jDoe',
    },
    {
      id: 5,
      employe_name: 'John Doe',
      employe_no: '000-00',
      username: 'jDoe',
    },
    {
      id: 6,
      employe_name: 'John Doe',
      employe_no: '000-00',
      username: 'jDoe',
    },
  ];

  headers = [
    { key: 'employe_no', title: 'Employee No.' },
    { key: 'username', title: 'Username' },
    { key: 'employe_name', title: 'Employee Name.' },
  ];

  ngOnInit(): void {}
}

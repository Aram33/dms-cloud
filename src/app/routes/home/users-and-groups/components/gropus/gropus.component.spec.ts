import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GropusComponent } from './gropus.component';

describe('GropusComponent', () => {
  let component: GropusComponent;
  let fixture: ComponentFixture<GropusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GropusComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GropusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

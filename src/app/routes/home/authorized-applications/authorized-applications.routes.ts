import { Routes } from '@angular/router';
import { AuthorizedApplications } from './authorized-applications.module';

export const routes: Routes = [
  {
    path: '',
    component: AuthorizedApplications,
  },
];

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-authorized-applications',
  templateUrl: './authorized-applications.component.html',
  styleUrls: ['./authorized-applications.component.scss'],
})
export class AuthorizedApplicationsComponent implements OnInit {
  files = [
    { id: 1, name: 'point', img: '<i class="fa fa-file-powerpoint-o"></i>' },
    { id: 2, name: 'word', img: '<i class="fa fa-file-word-o"></i>' },
    { id: 3, name: 'excel', img: '<i class="fa fa-file-excel-o"></i>' },
  ];

  constructor() {}

  ngOnInit(): void {}
}

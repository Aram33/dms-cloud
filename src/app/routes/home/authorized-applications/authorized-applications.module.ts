import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { routes } from '../home.routes';
import { AuthorizedApplicationsComponent } from './authorized-applications.component';

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [AuthorizedApplicationsComponent],
})
export class AuthorizedApplications {}

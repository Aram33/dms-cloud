import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorizedApplicationsComponent } from './authorized-applications.component';

describe('AuthorizedApplicationsComponent', () => {
  let component: AuthorizedApplicationsComponent;
  let fixture: ComponentFixture<AuthorizedApplicationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuthorizedApplicationsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AuthorizedApplicationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

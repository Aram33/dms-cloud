import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '@shared/material.module';
import { SharedModule } from '@shared/shared.module';
import { FavoritesComponent } from './favorites.component';
import { routes } from './favorites.routes';

@NgModule({
  imports: [SharedModule, MaterialModule, RouterModule.forChild(routes)],
  declarations: [FavoritesComponent],
})
export class FavoritesModule {}

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FavoritesComponent implements OnInit {
  fileContents = [
    {
      id: 1,
      name: 'Document 1',
      workspace: 'New Workspace.docx',
      pages: 'Pages 1',
    },
    {
      id: 2,
      name: 'Document 2',
      workspace: 'New Workspace.docx',
      pages: 'Pages 2',
    },
    {
      id: 3,
      name: 'Document 3',
      workspace: 'New Workspace.docx',
      pages: 'Pages 3',
    },
    {
      id: 4,
      name: 'Document 4',
      workspace: 'New Workspace.docx',
      pages: 'Pages 4',
    },
    {
      id: 5,
      name: 'Document 5',
      workspace: 'New Workspace.docx',
      pages: 'Pages 5',
    },
  ];

  constructor() {}

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.fileContents, event.previousIndex, event.currentIndex);
  }

  ngOnInit(): void {}
}

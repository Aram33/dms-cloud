import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { InfoComponent } from './components/info/info.component';
import { SharedModule } from '@shared/shared.module';
import { WorkspaceComponent } from './components/workspace/workspace.component';
import { SettingsComponent } from './components/settings/settings.component';

@NgModule({
  imports: [SharedModule],
  declarations: [
    DashboardComponent,
    WorkspaceComponent,
    InfoComponent,
    SettingsComponent,
  ],
})
export class DashboardModule {}

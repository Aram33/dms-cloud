import { Routes } from '@angular/router';
import { WorkflowComponent } from './workflow.component';

export const routes: Routes = [{ path: '', component: WorkflowComponent }];

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './workflow.routes';
import { SharedModule } from '@shared/shared.module';
import { WorkflowComponent } from './workflow.component';

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [WorkflowComponent],
})
export class WorkFlowModule {}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { ManageComponent } from './manage.component';
import { routes } from './manage.routes';
import { MyFilesComponent } from './my-files/my-files.component';
import { MatTreeModule } from '@angular/material/tree';
import { MaterialModule } from '../../shared/material.module';
import { WorkspacesComponent } from './workspaces/workspaces.component';
import { ProjectsComponent } from './workspaces/projects/projects.component';
import { MeetingReportsComponent } from './workspaces/projects/meeting-reports/meeting-reports.component';

@NgModule({
  imports: [SharedModule, MaterialModule, RouterModule.forChild(routes)],
  declarations: [ManageComponent, MyFilesComponent, WorkspacesComponent, ProjectsComponent, MeetingReportsComponent],
  exports: [],
})
export class ManageModule {}

import { Routes } from '@angular/router';
import { MyFilesModule } from './my-files.module';

export const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./my-files.module').then(m => MyFilesModule),
  },
];

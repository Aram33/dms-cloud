import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { routes } from '../manage.routes';

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [],
  exports: [],
})
export class MyFilesModule {}

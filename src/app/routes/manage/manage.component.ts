import { ChangeDetectionStrategy, Component, OnInit, ViewChild } from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
  CdkDragHandle,
} from '@angular/cdk/drag-drop';
import { MatTable } from '@angular/material/table';

@Component({
  selector: 'app-manage',
  styleUrls: ['./manage.component.scss'],
  templateUrl: './manage.component.html',
})
export class ManageComponent {
  @ViewChild('table') table: MatTable<any>;

  dataSource = [
    {
      id: 1,
      name: 'Document 1',
      workspace: 'New Workspace.docx',
      pages: 'Pages 1',
    },
    {
      id: 2,
      name: 'Document 2',
      workspace: 'New Workspace.docx',
      pages: 'Pages 2',
    },
    {
      id: 3,
      name: 'Document 3',
      workspace: 'New Workspace.docx',
      pages: 'Pages 3',
    },
    {
      id: 4,
      name: 'Document 4',
      workspace: 'New Workspace.docx',
      pages: 'Pages 4',
    },
    {
      id: 5,
      name: 'Document 5',
      workspace: 'New Workspace.docx',
      pages: 'Pages 5',
    },
  ];

  workflows = [
    {
      id: 1,
      task: 'Document 1',
      assigned_users: './avatar',
      status: 'Pages 1',
      start_date: 'mm / dd / yy',
      priorityLevel: 4,
    },
    {
      id: 2,
      task: 'Document 2',
      assigned_users: './avatar',
      status: 'Pages 2',
      start_date: 'mm / dd / yy',
      priorityLevel: 5,
    },
    {
      id: 3,
      task: 'Document 5',
      assigned_users: './avatar',
      status: 'Pages 1',
      start_date: 'mm / dd / yy',
      priorityLevel: 4,
    },
    {
      id: 1,
      task: 'Document 1',
      assigned_users: './avatar',
      status: 'Pages 1',
      start_date: 'mm / dd / yy',
      priorityLevel: 4,
    },
    {
      id: 1,
      task: 'Document 1',
      assigned_users: './avatar',
      status: 'Pages 1',
      start_date: 'mm / dd / yy',
      priorityLevel: 4,
    },
  ];

  headers = [
    { key: 'task', title: 'Task' },
    { key: 'assigned_users', title: 'Assigned Users', type: 'img' },
    { key: 'status', title: 'Status' },
    { key: 'start_date', title: 'Start Date', type: 'date' },
    { key: 'priorityLevel', title: 'PriorityLevel' },
  ];

  dropTable(event: CdkDragDrop<any[]>) {
    const prevIndex = this.workflows.findIndex(d => d === event.item.data);
    moveItemInArray(this.workflows, prevIndex, event.currentIndex);
  }

  constructor() {}
}

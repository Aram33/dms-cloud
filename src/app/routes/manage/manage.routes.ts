import { Routes } from '@angular/router';
import { MyFilesComponent } from './my-files/my-files.component';
import { ManageComponent } from './manage.component';

export const routes: Routes = [
  {
    path: '',
    component: ManageComponent,
    children: [
      {
        path: 'my-files',
        component: MyFilesComponent,
      },
    ],
  },
];

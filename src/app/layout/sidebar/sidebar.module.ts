import { NgModule } from '@angular/core';
import { HomeMenuComponent } from './home-menu/home-menu.component';
import { ManageMenuComponent } from './manage-menu/manage-menu.component';
import { DetailedSearchComponent } from './detailed-search/detailed-search.component';
import { SystemInformationComponent } from './system-information/system-information.component';
import { SharedModule } from '@shared/shared.module';
import { MaterialModule } from '../../shared/material.module';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [SharedModule, MaterialModule, RouterModule],
  declarations: [
    HomeMenuComponent,
    ManageMenuComponent,
    DetailedSearchComponent,
    SystemInformationComponent,
  ],
  exports: [
    HomeMenuComponent,
    ManageMenuComponent,
    DetailedSearchComponent,
    SystemInformationComponent,
  ],
})
export class SidebarModule {}

import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { WorkspaceFlatNode, WorkspaceNode } from './models/workspace.model';
import { TREE_DATA } from './tree-data';

@Component({
  selector: 'app-manage-menu',
  templateUrl: './manage-menu.component.html',
  styleUrls: ['./manage-menu.component.scss'],
})
export class ManageMenuComponent implements AfterViewInit {
  selected = 'Workspace';
  foods = [
    { value: 'steak-0', viewValue: 'Workspace' },
    { value: 'pizza-1', viewValue: 'Pizza' },
    { value: 'tacos-2', viewValue: 'Tacos' },
  ];

  treeControl: FlatTreeControl<WorkspaceFlatNode>;
  treeFlattener: MatTreeFlattener<WorkspaceNode, WorkspaceFlatNode>;
  dataSource: MatTreeFlatDataSource<WorkspaceNode, WorkspaceFlatNode>;

  constructor(private router: Router) {
    this.treeFlattener = new MatTreeFlattener(
      this.transformer,
      this._getLevel,
      this._isExpandable,
      this._getChildren,
    );
    this.treeControl = new FlatTreeControl<WorkspaceFlatNode>(
      this._getLevel,
      this._isExpandable,
    );
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
    this.dataSource.data = TREE_DATA;
  }

  transformer = (node: WorkspaceNode, level: number) => {
    const flatNode = new WorkspaceFlatNode();

    flatNode.name = node.name;
    flatNode.url = node.url;
    flatNode.level = level;
    flatNode.expandable = !!node.children;
    return flatNode;
  };

  private _getLevel = (node: WorkspaceFlatNode) => node.level;

  private _isExpandable = (node: WorkspaceFlatNode) => node.expandable;

  private _getChildren = (node: WorkspaceNode): Observable<WorkspaceNode[]> =>
    of(node.children ? node.children : []);

  hasChild = (_: number, _nodeData: WorkspaceFlatNode) => _nodeData.expandable;

  ngAfterViewInit() {
    this.treeControl.expand(this.treeControl.dataNodes[21]);
    this.treeControl.expand(this.treeControl.dataNodes[24]);
    this.treeControl.expand(this.treeControl.dataNodes[25]);
  }

  navigateInManage(node) {
    if (node.url) {
      this.router.navigate([node.url]);
    }
    console.log(node);
    debugger;
  }

  takeValue(food) {
    console.log(food);
  }

  selectionChange(food) {
    console.log(food);
  }
}

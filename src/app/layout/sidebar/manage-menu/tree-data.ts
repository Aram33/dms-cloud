import { WorkspaceNode } from './models/workspace.model';

export const TREE_DATA: WorkspaceNode[] = [
  {
    name: 'Sections',
    children: [{ name: 'Apple' }, { name: 'Banana' }, { name: 'Fruit loops' }],
  },
  {
    name: 'Templates',
    children: [
      {
        name: 'Green',
        children: [
          {
            name: 'Broccoli',
            children: [
              { name: 'U55' },
              { name: 'U56' },
              { name: 'U58' },
              { name: 'U51' },
              { name: 'U578' },
              {
                name: 'Dudush',
                children: [
                  { name: 'Bubush 49' },
                  { name: 'Bubush 1' },
                  { name: 'Bubush 78' },
                  { name: 'Bubush 51' },
                  { name: 'Bubush 12' },
                  { name: 'Bubush 8' },
                  { name: 'Bubush 87' },
                ],
              },
            ],
          },
          { name: 'Brussels sprouts' },
        ],
      },
    ],
  },
  {
    name: 'Workspaces',
    // url: 'workspace',
    children: [
      { name: 'Documentation', url: '' },
      { name: 'Marketing', url: '' },
      {
        name: 'Projects',
        children: [
          { name: 'Meeting Reports', url: 'manage/my-files' },
          { name: 'Graphic Resources', url: 'graphic' },
        ],
      },
      {
        name: 'Test Live',
      },
      {
        name: 'Test Live 2',
      },
    ],
  },
  {
    name: 'Workflows',
    children: [
      {
        name: 'Task Workflows',
        children: [
          { name: 'In Progress', url: '' },
          { name: 'Stuck', url: '' },
          { name: 'Done', url: '' },
        ],
      },
      {
        name: 'Approval Workflows',
        children: [{ name: 'Pending' }, { name: 'Reject' }, { name: 'Approved' }],
      },
    ],
  },
];

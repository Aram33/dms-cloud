export class WorkspaceFlatNode {
  name: string;
  level: number;
  expandable: boolean;
  url?: string;
}

export class WorkspaceNode {
  name: string;
  children?: WorkspaceNode[];
  url?: string;
}

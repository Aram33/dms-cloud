import {
  ChangeDetectionStrategy,
  Component,
} from '@angular/core';

@Component({
  selector: 'app-home-menu',
  templateUrl: './home-menu.component.html',
  styleUrls: ['./home-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeMenuComponent {
  menu = [
    { value: 'dashboard', viewValue: 'Dashboard', url: 'home/dashboard' },
    {
      value: 'authorized_applications',
      viewValue: 'Authorized Applications',
      url: 'home/authorized-applications',
    },
    {
      value: 'users_and_groups',
      viewValue: 'Users and Groups',
      url: 'home/users-and-groups',
    },
    { value: 'workflow', viewValue: 'Workflow', url: 'home/workflow' },
    { value: 'favorites', viewValue: 'Favorites', url: 'home/favorites' },
  ];
}

import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { NavigationStart, Router } from '@angular/router';
import { filter } from 'rxjs';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SidebarComponent implements OnInit {
  @ViewChild('manageContainerRef', { read: ViewContainerRef, static: true })
  viewContainer: ViewContainerRef;

  sidebarMenu = [
    {
      groupUrl: 'home',
      tabUrl: 'dashboard',
    },
    { groupUrl: 'manage', tabUrl: 'my-files' },
    { groupUrl: 'search', tabUrl: 'results' },
  ];

  activeGroup: number;
  activeTab: number;

  constructor(private router: Router) {}

  ngOnInit() {
    // look this gnac state
    this.router.events
      .pipe(filter(event => event instanceof NavigationStart))
      .subscribe((event: any) => {
        console.log(this);
        this.activeGroup = this.sidebarMenu.findIndex(
          v => `${v.groupUrl}/${v.tabUrl}` === event.url.slice(1),
        );

        // this.activeGroup = this.sidebarMenu.findIndex(
        //   v => v.groupUrl === event.url.slice(1),
        // );
      });
  }

  changeIndex(event) {
    console.log(event);
  }

  changeTab(event: MatTabChangeEvent) {
    this.activeGroup = event.index;
    const menuItem = this.sidebarMenu[event.index];
    let url = `${menuItem.groupUrl}`;

    if (menuItem.tabUrl) {
      url += `/${menuItem.tabUrl}`;
    }

    this.router.navigate([url]);
  }

  getCurrentUrl() {}
}

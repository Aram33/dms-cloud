import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./../notification-inbox.scss'],
})
export class InboxComponent implements OnInit {
  inboxList = [
    { id: 1, user: 'Username 1', description: 'Lorem ipsum dolor', isViewed: false },
    { id: 2, user: 'Username 2', description: 'Lorem ipsum dolor', isViewed: false },
    { id: 3, user: 'Username 3', description: 'Lorem ipsum dolor', isViewed: true },
  ];

  constructor() {}

  ngOnInit(): void {}
}

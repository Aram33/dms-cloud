import { NotificationComponent } from './notification/notification.component';
import { InboxComponent } from './inbox/inbox.component';
import {
  Component,
  ComponentFactory,
  ComponentFactoryResolver,
  ComponentRef,
  OnInit,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @ViewChild('notificationContainerRef', { read: ViewContainerRef, static: true })
  viewContainer: ViewContainerRef;
  activeNotification: ComponentRef<NotificationComponent | InboxComponent> | null;

  foods = [
    { value: 'steak-0', viewValue: 'Steak' },
    { value: 'pizza-1', viewValue: 'Pizza' },
    { value: 'tacos-2', viewValue: 'Tacos' },
  ];

  selected = 0;

  constructor(private cfr: ComponentFactoryResolver) {}

  showNotification(componentName: string) {
    this.viewContainer.clear();
    if (this.activeNotification?.componentType.name === componentName) {
      this.activeNotification = null;
    } else {
      let componentFactory: ComponentFactory<NotificationComponent | InboxComponent>;
      if (componentName === 'InboxComponent') {
        componentFactory = this.cfr.resolveComponentFactory(InboxComponent);
      } else {
        componentFactory = this.cfr.resolveComponentFactory(NotificationComponent);
      }

      this.activeNotification = this.viewContainer.createComponent(componentFactory);
    }
  }

  ngOnInit() {
    console.log();
  }

  selectionChange(event) {
    console.log(event);
  }

  takeValue(food) {
    console.log(food);
  }
}

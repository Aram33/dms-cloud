import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./../notification-inbox.scss'],
})
export class NotificationComponent implements OnInit {
  notificationTypes = [
    {
      description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,`,
      actions: [
        {
          id: 1,
          action: 'Action 1',
        },
        {
          id: 2,
          action: 'Action 2',
        },
        {
          id: 3,
          action: 'Action 3',
        },
      ],
    },
    {
      description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,`,
      actions: [
        {
          id: 1,
          action: 'Action 1',
        },
        {
          id: 2,
          action: 'Action 2',
        },
        {
          id: 3,
          action: 'Action 3',
        },
      ],
    },
    {
      description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,`,
      actions: [
        {
          id: 1,
          action: 'Action 1',
        },
        {
          id: 2,
          action: 'Action 2',
        },
        {
          id: 3,
          action: 'Action 3',
        },
      ],
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}

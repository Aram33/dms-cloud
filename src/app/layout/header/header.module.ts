import { NgModule } from '@angular/core';
import { NotificationComponent } from './notification/notification.component';
import { InboxComponent } from './inbox/inbox.component';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  imports: [SharedModule],
  declarations: [NotificationComponent, InboxComponent],
  exports: [NotificationComponent, InboxComponent],
  entryComponents: [NotificationComponent, InboxComponent],
})
export class HeaderModule {}

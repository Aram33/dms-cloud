import { Routes } from '@angular/router';
import { NotFoundRouteComponent } from './layout/not-found-route/not-found-route.component';

export const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./routes/home/home.module').then(m => m.HomeModule),
  },
  {
    path: 'manage',
    loadChildren: () => import('./routes/manage/manage.module').then(m => m.ManageModule),
  },
  {
    path: 'search',
    loadChildren: () => import('./routes/search/search.module').then(m => m.SearchModule),
  },
  { path: '', redirectTo: 'home/dashboard', pathMatch: 'full' },
  { path: '**', component: NotFoundRouteComponent, pathMatch: 'full' },
];

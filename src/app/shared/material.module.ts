import { NgModule } from '@angular/core';

import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTreeModule } from '@angular/material/tree';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import { CdkTreeModule } from '@angular/cdk/tree';
import { MatTableModule } from '@angular/material/table';

@NgModule({
  imports: [
    DragDropModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    MatFormFieldModule,
    MatTreeModule,
    CdkTreeModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatPaginatorModule,
  ],
  exports: [
    DragDropModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    MatFormFieldModule,
    CdkTreeModule,
    MatTreeModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatPaginatorModule,
  ],
})
export class MaterialModule {}

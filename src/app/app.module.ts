import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { HeaderComponent } from './layout/header/header.component';

import { MaterialModule } from './shared/material.module';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreRouterConnectingModule } from '@ngrx/router-store';

import { environment } from 'src/environments/environment';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { SharedModule } from '@shared/shared.module';
import { SidebarModule } from './layout/sidebar/sidebar.module';
import { RouterModule } from '@angular/router';
import { routes } from './app.routes';
import { HeaderModule } from './layout/header/header.module';
import { NotFoundRouteComponent } from './layout/not-found-route/not-found-route.component';

@NgModule({
  declarations: [AppComponent, HeaderComponent, SidebarComponent, NotFoundRouteComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SidebarModule,
    HeaderModule,
    MaterialModule,
    SharedModule,
    RouterModule.forRoot(routes, { useHash: true }),
    StoreModule.forRoot([]),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      logOnly: environment.production,
    }),
    StoreRouterConnectingModule.forRoot({}),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

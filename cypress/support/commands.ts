/// <reference types="cypress" />
// ***********************************************
// This example commands.ts shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//

const homeMenu = [
  'Dashboard',
  'Authorized Applications',
  'Users and Groups',
  'Workflow',
  'Favorites',
];

declare namespace Cypress {
  interface Chainable<Subject = any> {
    checkSidebarItems(index: number);
  }
}

function checkSidebarItems(index: number): void {
  cy.get('.home-menu-group > li').contains(homeMenu[index]).click();
  cy.contains(`${homeMenu[index]}`).should('class', 'active');

  // cy.url().then(url => {
  //   const path = url.split('/')[url.split('/').length - 2];
  //   const childPath = url.split('/')[url.split('/').length - 1];
  // });
}

Cypress.Commands.add('checkSidebarItems', checkSidebarItems);

//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
//
// declare global {
//   namespace Cypress {
//     interface Chainable {
//       login(email: string, password: string): Chainable<void>
//       drag(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       dismiss(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       visit(originalFn: CommandOriginalFn, url: string, options: Partial<VisitOptions>): Chainable<Element>
//     }
//   }
// }

describe('Dashboard', () => {
  it('Dashboard must have', () => {
    cy.visit('http://localhost:4200/#/home/dashboard');
    cy.get('.logo-container').contains(`Company name's workspace`);
    cy.get('.select-container').click();
    cy.get('.mat-option').eq(2).click();
    cy.get('.select-container').contains('Pizza');
  });
});

describe('Authorized Application', () => {
  it('Authorized Applications should have title', () => {
    cy.visit('http://localhost:4200/#/home/authorized-applications');
    cy.get('.title').contains(`Authorized Applications`);
    cy.get('.file').find('.fa-file-powerpoint-o').should('class', 'fa fa-file-powerpoint-o', '');
  });
});

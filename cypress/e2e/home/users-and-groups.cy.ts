describe('Dashboard', () => {
  it('Dashboard must have', () => {
    cy.visit('http://localhost:4200/#/home/users-and-groups');
    cy.get('.link.active').contains('User');
    // cy.contains('Groups').click();
    // cy.get('href=""').contains('Groups');
    // look this > ay senc hrefov kam urish attributeov kgres!!!
    cy.get('a[href*="home/users-and-groups/groups"]').click();
    cy.get('.link.active').contains('Groups');
    cy.get('.home-menu-group > li.active').contains('Users and Groups');
  });
});

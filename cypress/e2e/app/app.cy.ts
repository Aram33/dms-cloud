describe('App', () => {
  it('Application should have header', () => {
    cy.visit('http://localhost:4200/#/');
    cy.get('.header-container').contains(`Company name's workspace`);
    cy.get('.select-container').click();
    cy.get('mat-option').eq(1).click();
    cy.get('.select-container').contains(`Steak`);
  });

  it('Application should have Home menu in navigation sidebar', () => {
    cy.visit('http://localhost:4200/#/');
    for (let i = 1; i < 4; i++) {
      cy.checkSidebarItems(i);
    }
  });

  it('Application should have Manage menu in navigation sidebar', () => {
    cy.visit('http://localhost:4200/#/');
    cy.get('.mat-tab-label').eq(1).click();

    cy.get('.mat-tab-label-active').contains('Manage');
  });

  it('Application should have Search menu in navigation sidebar', () => {
    cy.visit('http://localhost:4200/#/');
    cy.get('.mat-tab-label').eq(2).click();

    cy.get('.mat-tab-label-active').contains('Search');
  });
});
